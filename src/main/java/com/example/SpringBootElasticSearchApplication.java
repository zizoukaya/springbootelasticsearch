package com.example;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import com.example.domain.Email;
import com.example.domain.Person;
import com.example.elastic.ElasticSearchConfiguration;
import com.example.service.IPersonService;

@SpringBootApplication
@Configuration
@ComponentScan(basePackages = "com.example")
@EnableAutoConfiguration(exclude = {ElasticSearchConfiguration.class})
public class SpringBootElasticSearchApplication implements CommandLineRunner {

    private static final Logger logger = LoggerFactory.getLogger(SpringBootElasticSearchApplication.class);
    
    @Autowired
    private IPersonService personService;
    
    private String gmail1 = "1ahmetkaya@gmail.com";
    private String gmail2 = "2ahmetkaya@gmail.com";
    private String hotmail1 = "1ahmetkaya@hotmail.com";
    private String hotmail2 = "2ahmetkaya@hotmail.com";
    
	public static void main(String[] args) {
		SpringApplication.run(SpringBootElasticSearchApplication.class, args);
	}
	

    private void addSomePersons() {
        Person person1 = getFirstPerson();
        personService.save(person1);

        Person person2 = getSecondPerson();
        personService.save(person2);
    }

    private Person getFirstPerson() {
        Person firstPerson = new Person();
        firstPerson.setId("1");
        firstPerson.setName("Ahmet Kaya Gmail");

        List<Email> emails = new ArrayList<Email>();
        emails.add(new Email("1", "1ahmetkaya@gmail.com"));
        emails.add(new Email("2", "2ahmetkaya@gmail.com"));
        firstPerson.setEmails(emails);
        
        return firstPerson;
    }
    
    private Person getSecondPerson() {
        Person secondPerson = new Person();
        secondPerson.setId("2");
        secondPerson.setName("Ahmet Kaya Hotmail");

        List<Email> emails = new ArrayList<Email>();
        emails.add(new Email("3", "1ahmetkaya@hotmail.com"));
        emails.add(new Email("4", "2ahmetkaya@hotmail.com"));
        secondPerson.setEmails(emails);
        
        return secondPerson;
    }


    public void run(String... args) throws Exception {
        addSomePersons();
        
        Page<Person> gmailPersonList = personService.findByEmailsAddress(gmail1, new PageRequest(0,10));
        logger.info("Content of gmail name query is {}", gmailPersonList.getContent());

        Page<Person> hotmailPersonList = personService.findByEmailsAddress(hotmail2, new PageRequest(0,10));
        logger.info("Content of hotmail name query is {}", hotmailPersonList.getContent());

    }
}
