package com.example.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.example.domain.Person;

public interface PersonRepository extends ElasticsearchRepository<Person, String>{

    Page<Person> findByEmailsAddress(String name, Pageable pageable);
}
