package com.example.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import com.example.domain.Person;

public interface IPersonService {
	Person save(Person person);
	Person findOne(String id);
    Iterable<Person> findAll();

    Page<Person> findByEmailsAddress(String tagName, PageRequest pageRequest);
}
