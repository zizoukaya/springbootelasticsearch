package com.example.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.example.domain.Person;
import com.example.repository.PersonRepository;

@Service
public class PersonService implements IPersonService {

	@Autowired
	private PersonRepository personRepository;
	
	@Override
	public Person save(Person person) {
		return personRepository.save(person);
	}

	@Override
	public Person findOne(String id) {
		return personRepository.findOne(id);
	}

	@Override
	public Iterable<Person> findAll() {
		return personRepository.findAll();
	}

	@Override
	public Page<Person> findByEmailsAddress(String address, PageRequest pageRequest) {
		return personRepository.findByEmailsAddress(address, pageRequest);
	}

}
