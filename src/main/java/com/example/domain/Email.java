package com.example.domain;

public class Email {

	private String id;
    private String address;
    
    public Email() {
    	
	}
    
    public Email(String id, String address) {
		this.setId(id);
		this.setAddress(address);
	}
    
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}

    @Override
    public String toString() {
        return "Email{" +
                "id=" + id +
                ", address='" + address +
                '}';
    }
}
